// @flow

const STORAGE_KEY = 'auth_token';

export function setToken(token) {
  localStorage.setItem(STORAGE_KEY, token);
}

export function deleteToken(token) {
  localStorage.removeItem(STORAGE_KEY);
}
