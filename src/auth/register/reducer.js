import * as ActionTypes from './actions';
import { isFilled, validateForm } from '../../components/Form/validate';

const initialState = {
  fields: {
    name: '',
    email: '',
    password: '',
  },
  errors: null,
  sending: false,
  sent: false,
  sendingError: null,
};

const validationRules = {
  email: isFilled('Email must be filled'),
  password: isFilled('Password must be filled'),
  name: isFilled('Name must be filled'),
};

export default function(state = initialState, action = {}) {
  switch (action.type) {
    case ActionTypes.VALUE_UPDATE:
      return {
        ...state,
        fields: {
          ...state.fields,
          [action.payload.field]: action.payload.value,
        },
      };

    case ActionTypes.VALIDATE:
      return {
        ...state,
        errors: validateForm(validationRules)(state.fields),
      };

    case ActionTypes.REGISTER:
      return {
        ...state,
        sending: true,
        sent: false,
      };

    case ActionTypes.REGISTER_SUCCESS:
      return {
        ...state,
        sending: false,
        sent: true,
      };

    case ActionTypes.REGISTER_FAIL:
      return {
        ...state,
        sending: false,
        sendingError: action.payload,
      };

    default:
      return state;
  }
}
