import React from 'react';
import Form from '../../../components/Form/Form';
import Input from '../../../components/Form/Input';
import SubmitButton from '../../../components/Form/SubmitButton';
import { Link } from 'react-router-dom';

const RegisterForm = ({
  fields,
  validationErrors,
  register,
  changeValue,
  sending,
  onFormSubmit,
}) => {
  return (
    <Form onSubmit={onFormSubmit}>
      <Input
        label="Email"
        type="email"
        id="register-email"
        validationError={validationErrors.email}
        autoComplete="email"
        onChange={value => changeValue('email', value)}
        value={fields.email}
      />

      <Input
        label="Name"
        type="text"
        id="register-name"
        validationError={validationErrors.name}
        autoComplete="name"
        onChange={value => changeValue('name', value)}
        value={fields.name}
      />

      <Input
        label="Password"
        type="password"
        id="register-pass"
        validationError={validationErrors.password}
        onChange={value => changeValue('password', value)}
        value={fields.password}
      />

      <div className="form__buttonRow form__buttonRow--login">
        <Link to="/login">Login</Link>
        <SubmitButton disabled={sending}>
          Register
        </SubmitButton>
      </div>
    </Form>
  );
};

export default RegisterForm;
