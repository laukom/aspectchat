import { setToken } from '../token';

export const VALUE_UPDATE = 'VALUE_UPDATE';
export const VALIDATE = 'VALIDATE';
export const REGISTER = 'REGISTER';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAIL = 'REGISTER_FAIL';

export const changeValue = (field, value) => ({
  type: VALUE_UPDATE,
  payload: {
    field,
    value,
  },
});

export const validate = () => ({
  type: VALIDATE,
});

const registerSuccess = userId => {
  setToken(userId);

  return {
    type: REGISTER_SUCCESS,
    payload: userId,
  };
};

export const register = () => (dispatch, getState) => {
  const formFields = getState().register.fields;

  dispatch(validate());

  if (!getState().register.errors) {
    const body = {
      name: formFields.name,
      email: formFields.email,
      password: formFields.password,
    };

    dispatch({
      type: REGISTER,
    });

    fetch('https://aspectchat.herokuapp.com/users', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    })
      .then(response => response.json())
      .then(payload => {
        dispatch(registerSuccess(payload.id));
      });
  }
};
