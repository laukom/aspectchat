import React from 'react';
import { connect } from 'react-redux';
import { register, changeValue } from './actions';
import RegisterForm from './components/RegisterForm';

import '../login/login.css';

const RegisterPage = ({
  fields,
  validationErrors,
  isLoggedIn,
  register,
  sending,
  changeValue,
  location,
}) => {
  validationErrors = validationErrors || {};

  const formSubmit = e => {
    e.preventDefault();

    register();
  };

  return (
    <div className="login__container">
      <div className="login__content">
        <h1 className="login__title">Register</h1>

        <RegisterForm
          fields={fields}
          validationErrors={validationErrors}
          changeValue={changeValue}
          onFormSubmit={formSubmit}
          sending={sending}
        />
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  fields: state.register.fields,
  validationErrors: state.register.errors,
  sending: state.register.sending,
  sent: state.register.sent,
  isLoggedIn: state.auth.isLoggedIn,
});

export default connect(
  mapStateToProps,
  { register, changeValue }
)(RegisterPage);
