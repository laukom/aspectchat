import React from 'react';
import { connect } from 'react-redux';
import { login, changeValue } from '../actions';
import Input from '../../../components/Form/Input';
import { Link } from 'react-router-dom';

const LoginForm = ({
  sending,
  login,
  emailValue,
  passwordValue,
  changeValue,
  errors,
  loginError,
}) => (
  <form
    className="form"
    onSubmit={event => {
      event.preventDefault();
      login();
    }}
  >
    {loginError && (
      <p style={{ color: 'red' }}>
        Login Failed! Please try again..
      </p>
    )}

    <Input
      label="E-mail"
      type="email"
      id="login-email"
      onChange={value => changeValue('email', value)}
      value={emailValue}
      validationError={errors.email}
    />

    <Input
      label="Password"
      type="password"
      id="login-password"
      onChange={value => changeValue('password', value)}
      value={passwordValue}
      validationError={errors.password}
    />

    <div className="form__buttonRow form__buttonRow--login">
      <Link to="/register">Register</Link>

      <button
        type="submit"
        className="form__submit"
        disabled={sending}
      >
        Login
      </button>
    </div>
  </form>
);

const mapStateToProps = state => {
  return {
    sending: state.login.sending,
    emailValue: state.login.emailValue,
    passwordValue: state.login.passwordValue,
    errors: state.login.errors,
    loginError: state.login.loginSubmitError,
  };
};

export default connect(
  mapStateToProps,
  { login, changeValue }
)(LoginForm);
