import React from 'react';
import LoginForm from './components/LoginForm';
import './login.css';
import styled from 'react-emotion';

const Container = styled('div')`
  align-self: center;
`;
const Content = styled('div')`
  background-color: white;
  border: 1px solid rgb(223, 231, 239);
  border-radius: 6px;
  padding: 20px;
`;

const Title = styled('h1')`
  margin: 0 0 40px;
`;

const LoginPage = () => {
  return (
    <Container>
      <Content>
        <Title>Login</Title>

        <LoginForm />
      </Content>
    </Container>
  );
};

export default LoginPage;
