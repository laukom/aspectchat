export const LOGIN_SUBMIT = 'LOGIN_SUBMIT';
export const LOGIN_SUBMIT_SUCCESS = 'LOGIN_SUBMIT_SUCCESS';
export const LOGIN_SUBMIT_ERROR = 'LOGIN_SUBMIT_ERROR';
export const CHANGE_VALUE = 'CHANGE_VALUE';

export const login = () => {
  return (dispatch, getState) => {
    // 1.
    dispatch({ type: LOGIN_SUBMIT });

    const someErrors = Object.values(
      getState().login.errors
    ).some(value => value !== null);

    if (!someErrors) {
      console.log({
        email: getState().login.emailValue,
        password: getState().login.passwordValue,
      });
      // 2.
      fetch('http://localhost:8080/login', {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify({
          email: getState().login.emailValue,
          password: getState().login.passwordValue,
        }),
      })
        .then(response => {
          if (response.status > 399) {
            throw new Error(
              'Login failed ' + response.status
            );
          }

          return response.json();
        })
        .then(
          payload => {
            dispatch({
              type: LOGIN_SUBMIT_SUCCESS,
              payload: payload.id,
            });
          },
          error => {
            dispatch({
              type: LOGIN_SUBMIT_ERROR,
              error,
            });
          }
        );
    }

    // // 2.
    // fetch('/login')

    // .then
    //   // 3.
    //   dispatch(LOGIN SUCCESS)

    //   // 4.
    //   dispatch(LOGIN ERROR)
  };
};

export const changeValue = (fieldType, value) => ({
  type: CHANGE_VALUE,
  value,
  fieldType,
});
