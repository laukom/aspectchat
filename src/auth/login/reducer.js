import {
  LOGIN_SUBMIT,
  CHANGE_VALUE,
  LOGIN_SUBMIT_ERROR,
  LOGIN_SUBMIT_SUCCESS,
} from './actions';

const initialState = {
  sending: false,
  emailValue: '',
  passwordValue: '',
  errors: {
    email: null,
    password: null,
  },
  loginSubmitError: null,
};

export default function reducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case LOGIN_SUBMIT:
      const errors = {
        ...initialState.errors,
      };
      if (state.emailValue.trim() === '') {
        errors.email = 'Please fill email!';
      }
      if (state.passwordValue.trim() === '') {
        errors.password = 'Please fill password';
      }

      return {
        ...state,
        errors,
        sending:
          !errors.email && !errors.password ? true : false,
      };

    case LOGIN_SUBMIT_ERROR:
      return {
        ...state,
        loginSubmitError: action.error,
        sending: false,
      };

    case LOGIN_SUBMIT_SUCCESS:
      return {
        ...state,
        sending: false,
        loginSubmitError: null,
      };

    case CHANGE_VALUE:
      console.log(action);
      return {
        ...state,
        [`${action.fieldType}Value`]: action.value,
      };

    default:
      return state;
  }
}
