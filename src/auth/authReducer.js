import { REGISTER_SUCCESS } from './register/actions';
import { LOGIN_SUBMIT_SUCCESS } from './login/actions';

const initialState = {
  isLoggedIn: false,
  currentUserId: null,
};

const authReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case REGISTER_SUCCESS:
      return {
        isLoggedIn: true,
        currentUserId: action.payload,
      };

    case LOGIN_SUBMIT_SUCCESS:
      return {
        isLoggedIn: true,
        currentUserId: action.payload,
      };

    default:
      return state;
  }
};

export default authReducer;
