import React, { Component } from 'react';
import Header from './components/Header';
import MessageBoard from './messages/MessageBoard';
import ProfilePage from './profile/ProfilePage';
import LoginPage from './auth/login/LoginPage';
import RegisterPage from './auth/register/RegisterPage';
import './store';
import {
  BrowserRouter,
  Route,
  Redirect,
  Switch,
} from 'react-router-dom';
import { connect } from 'react-redux';

import './styles/reset.css';
import './styles/main.css';
import './styles/form.css';

class App extends Component {
  state = {
    error: null,
    // ...
  };

  componentDidCatch(error) {
    this.setState({
      error,
    });
  }

  render() {
    return (
      <BrowserRouter>
        <div className="App container">
          <Route
            path="/(login|register)"
            children={({ match }) => {
              if (this.props.isLoggedIn) {
                return <Redirect to="/" />;
              }

              if (!match) {
                return <Header data={{ a: 1 }} />;
              }
              return null;
            }}
          />

          <div className="main">
            {this.state.error ? (
              <div
                style={{ height: '200px', color: 'red' }}
              >
                <h2>Error! {this.state.error.message}</h2>
              </div>
            ) : (
              <Switch>
                <Route
                  path="/thread/:threadId"
                  component={MessageBoard}
                />

                <Route
                  path="/login"
                  component={LoginPage}
                />
                <Route
                  path="/profile"
                  component={ProfilePage}
                />
                <Route
                  path="/register"
                  component={RegisterPage}
                />

                <Route
                  render={({ history, location }) => {
                    return (
                      <div>
                        <h2>
                          404 Not found: {location.pathname}
                        </h2>
                      </div>
                    );
                  }}
                />
              </Switch>
            )}
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default connect(state => ({
  isLoggedIn: state.auth.isLoggedIn,
}))(App);
