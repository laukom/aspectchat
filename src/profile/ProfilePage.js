import React from 'react';
import '../profile/profile.css';

const ProfilePage = () => {
  return (
    <div className="profile__container">
      <div className="profile__content">
        <form id="my_profile_form" className="profileForm">
          <header className="profileHeader">
            <img
              src="https://placeimg.com/256/256/people"
              width="256px"
              height="256px"
              alt="Profile pic"
              className="profileHeader__img"
            />

            <h3 className="profileHeader__name">
              Arnold Schwarzenegger
            </h3>
            <input
              type="text"
              className="profileHeader__name__input form__input"
            />
          </header>

          <div className="form__inputWrap">
            <label className="form__label" htmlFor="email_input">
              E-mail
            </label>
            <input
              id="email_input"
              type="email"
              className="form__input"
            />
          </div>

          <div className="form__inputWrap form__inputWrap--edited">
            <label className="form__label" htmlFor="tel_input">
              Tel.
            </label>
            <input
              id="tel_input"
              type="tel"
              className="form__input"
            />
          </div>

          <div className="form__inputWrap form__inputWrap--edited">
            <label className="form__label" htmlFor="bio_input">
              Bio
            </label>
            <textarea id="bio_input" className="form__input" />
          </div>

          <div className="form__buttonRow">
            <button className="form__reset">Reset</button>
            <button className="form__submit">Save</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ProfilePage;
