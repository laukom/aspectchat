import React from 'react';
import { ThemeContext } from '../index';

const withTheme = WrappedComponent => props => (
  <ThemeContext.Consumer>
    {({ companyColor }) => (
      <WrappedComponent
        companyColor={companyColor}
        {...props}
      />
    )}
  </ThemeContext.Consumer>
);

export default withTheme;
