import * as React from 'react';
import PT from 'prop-types';
import Search from './Search';
import ThreadList from './ThreadList';
import { connect } from 'react-redux';
import { loadThreads } from '../../threads/actions';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

const GET_THREADS = gql`
  {
    allThreads {
      id
      title
      messages {
        id
      }
    }
  }
`;

class Sidebar extends React.Component {
  componentDidMount() {
    this.props.loadThreads();
  }

  render() {
    return (
      <aside className="sidebar">
        <Search />

        <Query query={GET_THREADS}>
          {({ error, loading, data }) => {
            if (error) {
              return 'Error!';
            }
            if (loading) {
              return 'Loading...';
            }

            return <ThreadList threads={data.allThreads} />;
          }}
        </Query>
      </aside>
    );
  }
}

Sidebar.propTypes = {
  items: PT.array.isRequired,
  loading: PT.bool.isRequired,
  loadThreads: PT.func.isRequired,
  error: PT.object,
};

const mapStateToProps = state => ({
  items: state.threads.items,
  loading: state.threads.loading,
  error: state.threads.error,
});

export default connect(
  mapStateToProps,
  { loadThreads }
)(Sidebar);
