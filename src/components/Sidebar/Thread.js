import React from 'react';
import { Link } from 'react-router-dom';
import {
  FormattedMessage,
  FormattedRelative,
} from 'react-intl';

const Thread = ({ thread, companyColor }) => {
  return (
    <li>
      <Link to={`/thread/${thread.id}`} className="thread">
        <h3
          className="thread__title"
          style={{ color: companyColor }}
        >
          {thread.title}
        </h3>
        <FormattedRelative
          value={new Date().getTime() - 100000}
        />
        <div className="thread__bottom">
          <span className="thread__messagesCount">
            <FormattedMessage
              id="messages"
              values={{
                messageCount: thread.messages.length,
              }}
            />
          </span>
        </div>
      </Link>
    </li>
  );
};

export default Thread;
