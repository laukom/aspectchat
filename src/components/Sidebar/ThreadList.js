import React from 'react';
import Thread from './Thread';
import withTheme from '../withTheme';

const ThreadList = ({ threads, companyColor }) => {
  return (
    <ul className="threads">
      {threads.map(thread => (
        <Thread
          key={thread.id}
          thread={thread}
          companyColor={companyColor}
        />
      ))}
    </ul>
  );
};

export default withTheme(ThreadList);
