import React from 'react';
import { css } from 'emotion';
import styled from 'react-emotion';

const searchClass = css`
  padding: 20px 10px;
  display: flex;
  border-bottom: 1px solid #dfe7ef;
  background-color: white;
`;

const StyledButton = styled('button')`
  flex: 0 0 40px;
  min-width: 40px;
`;

const BlueButton = styled(StyledButton)`
  background: blue;
`;

const Search = ({ big = true }) => {
  const searchInputClass = css({
    padding: big ? '1.5em 3em' : '0.5em 2em',
    borderRadius: '10px',
    flex: 1,
    marginRight: '10px',
    border: '1px solid #dfe7ef',
  });

  return (
    <div className={searchClass}>
      <input type="search" className={searchInputClass} />
      <BlueButton title="search">
        <span role="img" aria-label="search">
          🔎
        </span>
      </BlueButton>
    </div>
  );
};

export default Search;
