import React from 'react';
import ReactDOM from 'react-dom';

const Modal = () => {
  return ReactDOM.createPortal(
    <div>I am Modal</div>,
    document.getElementById('modals')
  );
};

export default Modal;
