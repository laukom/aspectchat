// @flow

import React from 'react';

const Logo = (props: { logo: string }) => {
  return <span className="logo">AspectChat</span>;
};

export default Logo;
