import React from 'react';
import Logo from './Logo';
import ProfileBox from './ProfileBox';
import { ThemeContext } from '../../index';

import './header.css';

const Header = ({ data }) => {
  return (
    <ThemeContext.Consumer>
      {theme => (
        <header className="header" style={{ background: theme.companyColor }}>
          <div className="header__left">
            <Logo />
          </div>

          <ProfileBox />
        </header>
      )}
    </ThemeContext.Consumer>
  );
};

export default Header;
