import React from 'react';
import Dropdown from '../Dropdown';

const ProfileBox = () => (
  <div className="profileBox">
    <Dropdown
      toggleComponent={
        <img
          src="https://placeimg.com/256/256/people"
          width="24px"
          height="24px"
          alt="Profile pic"
          className="profileBox__button__img"
        />
      }
      contentClass="profileBox__content"
    >
      <nav>
        <ul className="profileNav">
          <span className="profileNav__text">My Name</span>
          <li>
            <a className="profileNav__link">My Profile</a>
          </li>
          <li>
            <a className="profileNav__link">Sign out</a>
          </li>
        </ul>
      </nav>
    </Dropdown>
  </div>
);

export default ProfileBox;
