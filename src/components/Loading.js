import React from 'react';

const Loading = endpoint => WrappedComponent => {
  return class LoadingHOC extends React.Component {
    state = {
      loading: false,
      items: [],
    };

    componentDidMount() {
      this.setState({
        loading: true,
      });

      setTimeout(() => {
        fetch(
          `https://aspectchat.herokuapp.com/${endpoint}`
        )
          .then(response => response.json())
          .then(payload => {
            this.setState({
              items: payload,
              loading: false,
            });
          });
      }, 500);
    }

    render() {
      return (
        <WrappedComponent
          items={this.state.items}
          loading={this.state.loading}
          {...this.props}
        />
      );
    }
  };
};

export default Loading;
