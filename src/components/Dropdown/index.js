import React from 'react';
import PT from 'prop-types';
import isNodeInRoot from './isNodeInRoot';
import './dropdown.css';
import View from './View';

class Dropdown extends React.Component {
  state = {
    isOpen: false,
  };

  dropdownContainer = React.createRef();

  componentDidMount() {
    document.addEventListener('click', this.closeDropdown);
  }

  componentWillUnmount() {
    document.removeEventListener(
      'click',
      this.closeDropdown
    );
  }

  handleToggleClick = () => {
    this.setState(oldState => ({
      isOpen: !oldState.isOpen,
    }));
  };

  openDropdown = () => {
    this.setState({
      isOpen: true,
    });
  };

  closeDropdown = event => {
    if (
      !isNodeInRoot(
        event.target,
        this.dropdownContainer.current
      )
    ) {
      if (this.state.isOpen) {
        this.setState({
          isOpen: false,
        });
      }
    }
  };

  render() {
    const {
      toggleComponent,
      children,
      contentClass,
    } = this.props;

    return (
      <View
        dropdownContainer={this.dropdownContainer}
        handleToggleClick={this.handleToggleClick}
        isOpen={this.state.isOpen}
        contentClass={contentClass}
        toggleComponent={toggleComponent}
        children={children}
      />
    );
  }
}

Dropdown.propTypes = {
  toggleComponent: PT.element.isRequired,
  children: PT.node.isRequired,
  contentClass: PT.string,
};

export default Dropdown;
