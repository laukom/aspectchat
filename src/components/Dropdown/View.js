import React from 'react';
import cx from 'classnames';

const DropdownView = ({
  dropdownContainer,
  handleToggleClick,
  isOpen,
  contentClass,
  toggleComponent,
  children,
}) => (
  <div className="dropdown" ref={dropdownContainer}>
    <button className="dropdown__toggle" onClick={handleToggleClick}>
      {toggleComponent}
    </button>

    {isOpen && <div className={cx('dropdown__content', contentClass)}>{children}</div>}
  </div>
);

export default DropdownView;
