export const isFilled = errorMessage => val => (val.trim() === '' ? errorMessage : null);

export const validateForm = rules => fields => {
  const errors = Object.entries(rules).reduce((errors, [field, rule]) => {
    const error = rule(fields[field]);
    if (error) {
      errors[field] = error;
    }

    return errors;
  }, {});

  return Object.keys(errors).length ? errors : null;
};
