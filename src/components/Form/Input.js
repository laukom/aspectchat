import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Input = ({
  label,
  id,
  type = 'text',
  validationError,
  onChange,
  ...restProps
}) => {
  const handleChange = e => {
    return onChange(e.target.value);
  };

  return (
    <div className="form__inputWrap">
      <label
        className="form__label"
        htmlFor={`${id}-input`}
      >
        {label}
      </label>
      <input
        id={`${id}-input`}
        type={type}
        className={cx('form__input', {
          'form__input--hasError': validationError,
        })}
        onChange={handleChange}
        {...restProps}
      />

      {validationError && (
        <span className="form__error">
          {validationError}
        </span>
      )}
    </div>
  );
};

Input.propTypes = {
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['text', 'email', 'password']),
  validationError: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default Input;
