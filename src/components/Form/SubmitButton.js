import React from 'react';

const SubmitButton = ({ disabled, children }) => {
  return (
    <button className="form__submit" disabled={disabled}>
      {children}
    </button>
  );
};

export default SubmitButton;
