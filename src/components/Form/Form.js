import React from 'react';

const Form = props => {
  return <form className="form" {...props} />;
};

export default Form;
