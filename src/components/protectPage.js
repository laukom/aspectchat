import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const protectPage = WrappedComponent => {
  const ProtectedPage = ({ isLoggedIn, ...restProps }) => {
    // if (!isLoggedIn) {
    //   return <Redirect to="/login" />;
    // }

    return <WrappedComponent {...restProps} />;
  };

  return connect(state => ({
    isLoggedIn: state.auth.isLoggedIn,
  }))(ProtectedPage);
};

export default protectPage;
