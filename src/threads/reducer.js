import {
  LOAD_THREADS,
  LOAD_THREADS_SUCCESS,
  LOAD_THREADS_FAIL,
} from './actions';

const initialState = {
  loading: false,
  items: [],
  error: null,
};

const threadsReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_THREADS:
      return {
        ...state,
        loading: true,
        error: null,
      };

    case LOAD_THREADS_SUCCESS:
      return {
        ...state,
        items: action.payload,
        loading: false,
      };

    case LOAD_THREADS_FAIL:
      return {
        ...state,
        error: action.error,
        loading: false,
      };

    default:
      return state;
  }
};

export default threadsReducer;
