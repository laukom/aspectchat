export const LOAD_THREADS = 'LOAD_THREADS';
export const LOAD_THREADS_SUCCESS = 'LOAD_THREADS_SUCCESS';
export const LOAD_THREADS_FAIL = 'LOAD_THREADS_FAIL';

export const loadThreads = () => {
  return dispatch => {
    dispatch({
      type: LOAD_THREADS,
    });

    fetch('https://aspectchat.herokuapp.com/threads')
      .then(response => {
        if (response.status > 399) {
          throw new Error();
        }

        return response.json();
      })
      .then(payload => {
        dispatch(loadThreadsSuccess(payload));
      })
      .catch(error => {
        dispatch(loadThreadsFail(error));
      });
  };
};

const loadThreadsSuccess = payload => ({
  type: LOAD_THREADS_SUCCESS,
  payload,
});

const loadThreadsFail = error => ({
  type: LOAD_THREADS_FAIL,
  error,
});
