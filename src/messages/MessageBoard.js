import React, { Fragment } from 'react';
import Sidebar from '../components/Sidebar';
import MessageBlock from './components/MessageBlock';
import WriteMessage from './components/WriteMessage';
import Modal from '../components/Modal';
import '../messages/messages.css';
import protectPage from '../components/protectPage';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

export const GET_MESSAGES = gql`
  query getMessages($threadId: ID!) {
    allMessages(filter: { thread: { id: $threadId } }) {
      id
      text
      author {
        name
      }
      likes {
        id
      }
    }
  }
`;

const MessageBoard = ({ match }) =>
  console.log(match) || (
    <Fragment>
      <Sidebar />

      <Modal />

      <div className="content">
        <Query
          query={GET_MESSAGES}
          variables={{ threadId: match.params.threadId }}
          fetchPolicy="cache-and-network"
        >
          {({ loading, error, data }) => {
            if (loading) return <p>Still Loading..</p>;
            if (error) return <p>Error!</p>;

            return (
              <div className="messageBlocks">
                <MessageBlock
                  messages={data.allMessages}
                  date="10 Sept. 2018"
                />
              </div>
            );
          }}
        </Query>

        <WriteMessage />
      </div>
    </Fragment>
  );

export default protectPage(MessageBoard);
