import React from 'react';
import cx from 'classnames';

const likeIcon = (
  <svg
    className="message__like__icon"
    fillRule="evenodd"
    clipRule="evenodd"
    strokeLinejoin="round"
    strokeMiterlimit="1.414"
    aria-labelledby="title"
    viewBox="0 0 32 32"
    preserveAspectRatio="xMidYMid meet"
  >
    <g>
      <path
        d="M25.982 9.75c-2-2-6-3-10 1-4-4-8-3-10-1-8 8 7 17 10 17s18-9 10-17z"
        fillRule="nonzero"
      />
    </g>
  </svg>
);

const Like = ({ liked, likeCount }) => {
  return (
    <button
      className={cx('message__like', { 'message__like--liked': liked })}
      title={liked ? 'like' : 'un-like'}
    >
      {likeIcon}

      <span className="message__like__count">{likeCount}</span>
    </button>
  );
};

export default Like;
