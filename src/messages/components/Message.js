import React from 'react';
import cx from 'classnames';
import PT from 'prop-types';
import Like from './Like';

const Message = ({
  message: {
    text,
    ownMessage = false,
    author: { name },
    liked = false,
    likes,
  },
}) => {
  return (
    <div
      className={cx(
        'message',
        ownMessage ? 'message--me' : 'message--they'
      )}
    >
      <a className="message__userName">{name}</a>

      <div className="message__row">
        <div className="message__content">
          <p className="message__content__text">{text}</p>
        </div>

        <Like liked={liked} likeCount={likes.length} />
      </div>
    </div>
  );
};

export const MessageShape = PT.shape({
  text: PT.string.isRequired,
  ownMessage: PT.bool.isRequired,
  user: PT.shape({
    id: PT.number.isRequired,
    name: PT.string.isRequired,
  }).isRequired,
  liked: PT.bool.isRequired,
  likeCount: PT.number.isRequired,
});

Message.propTypes = {
  message: MessageShape.isRequired,
};

export default Message;
