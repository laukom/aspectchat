import React from 'react';
import PT from 'prop-types';
import Message, { MessageShape } from './Message';

const MessageBlock = ({ messages, date }) => {
  return (
    <section className="messageBlock">
      <header className="messageBlock__header">
        <hr />
        <span className="messageBlock__header__date">{date}</span>
        <hr />
      </header>

      <div className="messageBlock__messages">
        {messages.map(message => <Message key={message.id} message={message} />)}
      </div>
    </section>
  );
};

MessageBlock.propTypes = {
  messages: PT.arrayOf(MessageShape).isRequired,
  date: PT.string.isRequired,
};

export default MessageBlock;
