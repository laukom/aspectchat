import {
  createStore,
  combineReducers,
  applyMiddleware,
} from 'redux';
import loginReducer from './auth/login/reducer';
import registerReducer from './auth/register/reducer';
import authReducer from './auth/authReducer';
import threadsReducer from './threads/reducer';
import { composeWithDevTools } from 'redux-devtools-extension';

const globalReducer = combineReducers({
  auth: authReducer,
  login: loginReducer,
  register: registerReducer,
  threads: threadsReducer,
});

const loggingMiddleware = store => next => action => {
  console.log('PREV STATE', store.getState());
  console.log('ACTION', action);

  next(action);

  console.log('NEXT STATE', store.getState());
};

const thunkMiddleware = store => next => action => {
  if (typeof action === 'function') {
    action(store.dispatch, store.getState);
  } else {
    next(action);
  }
};

const store = createStore(
  globalReducer,
  composeWithDevTools(
    applyMiddleware(thunkMiddleware, loggingMiddleware)
  )
);

export default store;
