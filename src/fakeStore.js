const action = {
  type: 'INCREMENT',
};

function reducer(oldState, action) {
  if (action.type === 'INCREMENT') {
    return oldState + 1;
  }

  return oldState;
}

const createStore = () => {
  let state = 0;
  let listeners = [];

  return {
    getState: () => state,
    dispatch: action => {
      state = reducer(state, action);
      listeners.forEach(listener => {
        listener();
      });
    },
    subscribe: listener => {
      listeners.push(listener);
    },
  };
};

const store = createStore();

const newListener = () => {
  console.log('state changed', store.getState());
};
store.subscribe(newListener);

store.dispatch(action);
store.dispatch(action);
store.dispatch(action);
