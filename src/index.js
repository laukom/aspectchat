import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'whatwg-fetch';
import store from './store';
import { Provider } from 'react-redux';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider, addLocaleData } from 'react-intl';
import cs from 'react-intl/locale-data/cs';
import ApolloClient from 'apollo-boost';

addLocaleData(cs);

const client = new ApolloClient({
  uri:
    'https://api.graph.cool/simple/v1/cjifpn39t3u030167hqqwa55d',
});

export const ThemeContext = React.createContext();

const theme = {
  companyColor: 'green',
};

const messages = {
  messages: `{messageCount} {messageCount, plural,
    one {zprava}
    few {zpravy}
    other {zprav}
  }`,
};

ReactDOM.render(
  <IntlProvider locale="cs" messages={messages}>
    <ApolloProvider client={client}>
      <Provider store={store}>
        <ThemeContext.Provider value={theme}>
          <App />
        </ThemeContext.Provider>
      </Provider>
    </ApolloProvider>
  </IntlProvider>,
  document.getElementById('root')
);
